//
//  ViewController.swift
//  GeasturesGR2
//
//  Created by USRDEL on 30/6/17.
//  Copyright © 2017 USRDEL. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
   
    
    @IBOutlet weak var txtNumberTaps: UITextField!
    @IBOutlet weak var txtRojo: UITextField!
    @IBOutlet weak var txtAzul: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        tapLabel.isHidden=true
        tapGesture1.numberOfTapsRequired=2
        tapGesture1.numberOfTouchesRequired=5
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    var orgH, orgW, orgX, orgY: CGFloat!
    override func viewWillAppear(_ animated: Bool) {
        orgH=hconstraint.constant
        orgW=wconstraint.constant
        orgX=gesturesView.center.x
        orgY=gesturesView.center.y
    }
    @IBOutlet weak var touchesLabel: UILabel!
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
       
        
        let touch=touches.first!
        tapLabel.text="\(touch.tapCount ?? 0)"
        
        
        if tapLabel.text==txtNumberTaps.text{
            gesturesView.backgroundColor=UIColor.green
        }
        
        else if tapLabel.text==txtRojo.text{
            gesturesView.backgroundColor=UIColor.red
        }
        else if tapLabel.text==txtAzul.text{
            gesturesView.backgroundColor=UIColor.blue
        }
    }
    @IBOutlet weak var tapLabel: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var locationLabel: UILabel!
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch=touches.first
       
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

    @IBOutlet weak var gesturesView: UIView!
    
    @IBAction func tapGesture(_ sender: Any) {
        
        let gesture=sender as! UITapGestureRecognizer
        gesturesView.backgroundColor=UIColor.red
    }
    
    @IBOutlet var tapGesture1: UITapGestureRecognizer!
    @IBOutlet weak var wconstraint: NSLayoutConstraint!
    @IBOutlet weak var hconstraint: NSLayoutConstraint!
    @IBOutlet var pinchOnView: UIPinchGestureRecognizer!
    
    @IBAction func pinchOnView(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale)
        print(sender.velocity)
        hconstraint.constant=orgH*sender.scale
        wconstraint.constant=orgW*sender.scale
        if sender.velocity > 12{
            hconstraint.constant=view.frame.height
            wconstraint.constant=view.frame.width
            sender.isEnabled=false
        }
        if sender.state == .ended{
            orgH=hconstraint.constant
            orgW=wconstraint.constant
        }
        if sender.state == .cancelled {
            orgH=hconstraint.constant
            orgW=wconstraint.constant
            sender.isEnabled=true
        }
    }
    @IBAction func panOnView(_ sender: UIPanGestureRecognizer) {
        print(sender.translation(in: view))
        gesturesView.center.x=orgX+sender.translation(in: view).x
        gesturesView.center.y=orgY+sender.translation(in: view).y
        
        
        if sender.state == .ended
        {
            orgY=gesturesView.center.y
            orgX=gesturesView.center.x
        }
    
    }
}

